<?php
require 'app/Mage.php';
Mage::app();
Mage::app()->setCurrentStore(0);

$products = Mage::getModel("catalog/product")->getCollection();

foreach($products as $product) {
    try {
        $product->delete();
    } catch(Exception $e) {
        var_dump($e->getMessage());
    }
}