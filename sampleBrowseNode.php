<?php
/**
 * For a running Search Demo see: http://amazonecs.pixel-web.org
 */

if ("cli" !== PHP_SAPI)
{
    echo "<pre>";
}
ini_set("display_errors", 1);


if (is_file('sampleSettings.php'))
{
  include 'sampleSettings.php';
}

defined('AWS_API_KEY') or define('AWS_API_KEY', 'API KEY');
defined('AWS_API_SECRET_KEY') or define('AWS_API_SECRET_KEY', 'SECRET KEY');
defined('AWS_ASSOCIATE_TAG') or define('AWS_ASSOCIATE_TAG', 'ASSOCIATE TAG');

require 'lib/Amazon/ECS.php';
try
{
    $amazonEcs = new Amazon_ECS('AKIAJ25LFH3X4AIMCHUQ', 'TO295iN2YnDyE+alG4eLJS3+8HOAD584jEL/GBtq', 'com', 'examplecom017-20');

    // for the new version of the wsdl its required to provide a associate Tag
    // @see https://affiliate-program.amazon.com/gp/advertising/api/detail/api-changes.html?ie=UTF8&pf_rd_t=501&ref_=amb_link_83957571_2&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=&pf_rd_s=assoc-center-1&pf_rd_r=&pf_rd_i=assoc-api-detail-2-v2
    // you can set it with the setter function or as the fourth paramameter of ther constructor above
    $amazonEcs->associateTag(AWS_ASSOCIATE_TAG);

    // First of all you have to set an another ResponseGroup. If not the request would not be successful
    // Possible Responsegroups: BrowseNodeInfo,MostGifted,NewReleases,MostWishedFor,TopSellers
    $amazonEcs->responseGroup('BrowseNodeInfo');

    // Then browse a node like this:  nodeId (See: http://docs.amazonwebservices.com/AWSECommerceService/2010-09-01/DG/index.html?BrowseNodeIDs.html)
    // For example: 542064 on German Amazon is: Software
    $response = $amazonEcs->browseNodeLookup(542064);
    //var_dump($response);

    // The response contains now some information about this Node and its children, ancestors etc.
    // So we picked out one noteId of the childelements: 408306 -> Programmierung (Programming).
    // Now we want to browse this node

    $response = $amazonEcs->category('Software');
    //var_dump($response);

    // Searching something and returning some nodes found for this keyword.
    $response = $amazonEcs->returnType(1)->page(1)->responseGroup('ItemIds,Large')->similarityLookup('B00XH1F44G');
    var_dump($response);
    foreach($response["Items"]["Item"] as $item) {
        var_dump($item["ASIN"]);
        var_dump($item["ItemAttributes"]["Title"]);
    }
}
catch(Exception $e)
{
  echo $e->getMessage();
}

if ("cli" !== PHP_SAPI)
{
    echo "</pre>";
}
