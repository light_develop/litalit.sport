<?php

/**
 * Class Sereban_Product_Model_Attribute
 */
class Sereban_Product_Model_Attribute extends Sereban_Product_Model_Abstract
{
    const DEFAULT_FRONTEND_TYPE   = "text";
    const DEFAULT_BACKEND_TYPE    = "varchar";
    const SELECT_FRONTEND_TYPE    = "select";
    const PRODUCT_TYPE_CODE       = "catalog_product";

    /** @var  Mage_Eav_Model_Resource_Entity_Attribute_Collection */
    protected $_collection;
    protected $_entityTypeId = 4;
    /** @var  array */
    protected $_variationAttributes = array();
    /** @var array used only in varriation_attributes */
    private   $__options            = array();
    private   $__optionLabels       = array();

    /**
     * Return entity type id of catalog_product
     * @return int
     */
    protected function _getProductEntityTypeId() {
        /** @var Mage_Eav_Model_Entity_Type $entityTypeModel */
        $entityTypeModel = Mage::getSingleton("eav/entity_type");
        $entityTypeModel = $entityTypeModel->load(self::PRODUCT_TYPE_CODE, "entity_type_code"); //load model with entity type we need

        return $entityTypeModel && $entityTypeModel->getEntityTypeId() ? $entityTypeModel->getEntityTypeId() : $this->_entityTypeId;
    }

    /**
     * Setup collection
     */
    public function __construct() {
        $this->_entityTypeId = $this->_getProductEntityTypeId();
        $this->_collection   = $this
                                ->getCollection()
                                ->addFieldToFilter("entity_type_id", $this->_entityTypeId);
        /** @var array _variationAttributes -> standard variation attributes can be found in any column in table */
        $this->_variationAttributes = $this->_getHelper()->getVariationAttributes();

    }

    /**
     * @param array $attributes
     */
    public function addVariationAttributes(array $attributes) {
        foreach($attributes as $attribute) {
            $this->_variationAttributes[] = $attribute;
        }
    }

    /**
     * This function prepare data for attribute
     * @return Mage_Eav_Model_Entity_Attribute
     * @param string $attributeCode
     */
    public function prepareAttribute($attributeCode) {
        $isVariationAttribute = in_array($attributeCode, $this->_variationAttributes);
        /** @var Mage_Eav_Model_Entity_Attribute $attribute */
        $attribute     = $this->_collection->getItemByColumnValue("attribute_code", $attributeCode);

        /** If we have new attribute -> we should create it */
        if(empty($attribute)) {
            $attribute     = $this->_getAttributeInstance();
            $attribute->setData(array(
                "attribute_code"  => $attributeCode,
                "entity_type_id"  => $this->_entityTypeId,
                "type"            => self::DEFAULT_BACKEND_TYPE,
                "input"           => $isVariationAttribute ? self::SELECT_FRONTEND_TYPE : self::DEFAULT_FRONTEND_TYPE,
                "global"          => 1,
                "is_configurable" => $isVariationAttribute ? 1 : 0, //if it is variation we can use it in creating configurables
            ));

            try {
                /** cannot save attribute after this saving */
                $attribute->save();
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }

        /** If it`s variation attribute (variation or simple attribute) */
        if($isVariationAttribute && !$attribute->getData("option_labels")) {
            $attribute->setData("is_variation_attribute", true);
            $options = $attribute->getSource()->getAllOptions();
            $_labels = array();

            if(is_array($options) && count($options)) {
                foreach($options as $option) {
                    if(isset($option["label"]))
                            $_labels[] = $option["label"];
                }
            }
            /** set alreay existing options */
            $attribute->setData("option_labels", $_labels);
        }

        return $attribute;
    }

    /**
     * Saved option
     * @param string $attributeCode
     * @param string $option
     */
    public function prepareOptionAndAttribute($attributeCode, $option) {
        $attribute = $this->prepareAttribute($attributeCode);
        /** we should option */
        if($this->_getHelper()->optionNotExist($attribute, $option)) {
            $setup  = new Mage_Eav_Model_Entity_Setup('core_setup');
            $option = array(
                "attribute_id" => $attribute->getAttributeId(),
                "value"        => array(
                    (string)$option . "_key" => (string)$option
                )
            );

            try {
                $setup->addAttributeOption($option);
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }
    }


}