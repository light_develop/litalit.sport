<?php
abstract class Sereban_Product_Model_Product_Abstract extends Mage_Catalog_Model_Product
{
    const ATTRIBUTE_MAPPING_PATH = "amazon/product/map/attributes"; //there should be product type
    /** @var  string there should be product type */
    protected $_type;
    /** @var  Sereban_Product_Model_Attribute Initialize attribute instance with collection of all attributes */
    protected $_attributeInstance;
    /** @var  array */
    protected $_productData;

    abstract function processData();

    /**
     * @param $attributeCode
     * @return Mage_Eav_Model_Entity_Attribute
     */
    protected function _prepareAttribute($attributeCode) {
        return $this->_attributeInstance->prepareAttribute($attributeCode);
    }

    protected function _processMappingAttributes() {
        $_attributesMap = Mage::app()->getConfig()->getNode(self::ATTRIBUTE_MAPPING_PATH . DS . $this->_type)->asArray();

        if(is_array($_attributesMap)) {
            foreach($_attributesMap as $magentoAttributeCode => $_value) {
                /** @var string $_value -> path */
                $_value = $this->_getHelper()->prepareConfigValue($_value, $this->_productData);
                /** prepare attribute and option */
                $this->_attributeInstance->prepareOptionAndAttribute($magentoAttributeCode, $_value);
                $this->setData($magentoAttributeCode, $_value);
            }
        }
    }

    /**
     * @param array $productData
     */
    public function setProductData(array $productData) {
        $this->_productData = $productData;
    }

    /**
     * Recursive function
     * @param array | null | string $data
     * @return array | null | string
     */
    protected function _processNonMappingAttributes($data) {
        if(!is_array($data)) return $data;

        foreach($data as $magentoAttributeCode => $_value) {
            /** @var string | array | null $_value -> data to save */
            $_value = $this->_processNonMappingAttributes($_value);

            if($_value && !$this->getData($magentoAttributeCode)) {
                $this->_attributeInstance->prepareOptionAndAttribute($magentoAttributeCode, $_value);
                $this->setData($magentoAttributeCode, $_value);
            }
        }

        return $data;
    }

    /** @var  Mage_Eav_Model_Entity_Attribute */
    protected function _getAttributeInstance() {
        return Mage::getSingleton("eav/entity_attribute");
    }



    /**
     * @return Sereban_Product_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("sereban_product");
    }

}