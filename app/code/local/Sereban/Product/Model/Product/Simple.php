<?php
class Sereban_Product_Model_Product_Simple extends Sereban_Product_Model_Product_Abstract
{
    protected $_type = "simple";

    public function processData() {
        $this->_processMappingAttributes();
        $this->_processNonMappingAttributes($this->_productData);
    }
}