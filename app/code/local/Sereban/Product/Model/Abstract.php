<?php
abstract class Sereban_Product_Model_Abstract extends Mage_Core_Model_Abstract
{
    /** @var  Mage_Catalog_Model_Product */
    protected $_product;

    /** @var  Mage_Eav_Model_Entity_Attribute */
    protected function _getAttributeInstance() {
        return Mage::getSingleton("eav/entity_attribute");
    }

    public function setProduct(Mage_Catalog_Model_Product $product) {
        $this->_product = $product;
    }

    /**
     * @return Sereban_Product_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("sereban_product");
    }
}