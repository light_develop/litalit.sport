<?php
class Sereban_Product_Model_Image extends Sereban_Product_Model_Abstract
{
    const IMAGE_DELIMITER = ";";
    const IMAGE_PATH      = "amazon/product/map/attributes/images";
    const IMAGE_FOLDER    = "media/amazon";
    /** @var  array */
    protected $_imageData;

    /**
     * @return bool
     */
    public function processData() {
        if(!is_array($this->_imageData) || !$this->_product) return false;

        $config = Mage::app()->getConfig()->getNode(self::IMAGE_PATH);

        foreach($config as $magentoAttributeCode => $_path) {
            $imageUrl  = $this->_getHelper()->loopThroughArray($_path, $this->_imageData);
            $this->_getHelper()->createFolders(self::IMAGE_FOLDER . DS . $magentoAttributeCode); //create folder for each magento attribute
            $imagePath = self::IMAGE_FOLDER . DS . $magentoAttributeCode . DS . $this->_getNameByUrl($imageUrl);
            /** TODO: make CURL request in future */
            /** Making file_get_contents request : Note open external links as file option should be enable in php settings */
            $this->_makeRequest($imagePath, $imageUrl);

            try {
                $this->_product->addImageToMediaGallery($imagePath, $magentoAttributeCode, false, false);
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }

        return true;
    }

    /**
     * @param $url
     * @return string
     */
    protected function _getNameByUrl($url) {
        $url = explode("/", $url);
        return $url[count($url) - 1];
    }

    /**
     * @param string $url = external link to image
     * @param string $path = file with our image
     */
    protected function _makeRequest($path, $url) {
        $content = @file_get_contents($url);
        /** If we have new image and response is valid */
        if($content && strlen($content) && !file_exists($path)) {
            try {
                file_put_contents($path, $content);
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }
    }
}