<?php
class Sereban_Product_Helper_Data extends Mage_Core_Helper_Data
{
    /** see confi.xml */
    const VARIATION_ATTRIBUTES_PATH = "amazon/product/variation_attributes";
    const PATH_DELIMITER            = "/";
    /** Working with config.xml attributes */
    const PATH_OPEN_BRACE  = "{";
    const PATH_CLOSE_BRACE = "}";
    const PATH_XOR         = "|||";

    /**
     * Return attribute codes which should be like options
     * @return array
     */
    public function getVariationAttributes() {
        $variationAttributes = array();
        /** @var string $variations */
        $variations = (string)Mage::app()->getConfig()->getNode(self::VARIATION_ATTRIBUTES_PATH); //try to convert to string

        if($variations && strlen($variations) && preg_match("\,", $variations)) {
            $variationAttributes = explode(",", $variations);
        }

        return $variationAttributes;
    }

    /**
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * true -> not exist; false -> exist
     * @param string $option
     */
    public function optionNotExist(Mage_Eav_Model_Entity_Attribute $attribute, $option) {
        if(!$attribute->getIsVariationAttribute()) return false;
        /** @var array | null $labels */
        $labels = $attribute->getOptionLabels();

        if(!is_array($labels)) return false;

        return !in_array($option, $labels);
    }

    /**
     * @param $path
     * @param array $data
     * @return array|string|null
     */
    public function loopThroughArray($path, array $data) {
        $arrayPath     = explode(self::PATH_DELIMITER, $path); //array of all patch items

        while(count($arrayPath)) {
            $_item = array_shift($arrayPath); //new item
            if(!isset($data[$_item])) return null;

            $data = $data[$_item];
        }

        return $data;
    }

    /**
     * Detected if config element has path variable inside
     * @return boolean
     */
    protected function _isPath($value) {
        return preg_match("/" . self::PATH_OPEN_BRACE . ".+" . self::PATH_CLOSE_BRACE . "/", $value);
    }

    /**
     * XORED used when left value is null or array -> then we try to get property of righter value
     * @param $_value
     * @return array|null|string
     */
    public function prepareConfigValue($value, $data) {
        if($this->_isPath($value)) {
            $xors = explode(self::PATH_XOR, $value);

            foreach($xors as $xor) {
                $value = $this->loopThroughArray($xor, $data);
                if(!is_null($value) && !is_array($value))
                    break; //if it is normal value
            }
        }

        return $value;
    }

    /**
     * @param $folders
     */
    public function createFolders($folders) {
        if(!is_dir($folders)) {
            try {
                exec("mkdir -m 777 -p $folders");
            } catch(Exception $e) { //maybe we have wrong permissions
                exec("chown -R 777 $folders");
            }
        }
    }
}