<?php

/**
 * Class Sereban_Product_ProductController
 */
class Sereban_Product_ProductController extends Mage_Core_Controller_Front_Action
{
    protected $_simpleProductsArray = array();

    const CATALOG_PRODUCT = 'catalog_product';
    const PATH_TO_IMAGE = "media/amazon/images";

    /**
     * Create simple product
     */
    public function indexAction()
    {
        $collection = Mage::getSingleton("sereban_amazon/api_response_chunk_item")->getCollection();

        foreach ($collection as $item) {
            var_dump($item->getData());
        }
        die('Hello  ');
    }

    public function createSimpleProductAction()
    {
        $this->_createProduct(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);

    }


    /**
     * Creating configurable product
     */
    public function createConfProductAction()
    {

        $configPoductdta = new Mage_Core_Model_Resource();
        $read = $configPoductdta->getConnection('core_read');
        $skus = array();
        $products = Mage::getSingleton("catalog/product")->getCollection()->addAttributeToSelect("sku");

        foreach ($products as $_p) {
            $skus[] = $_p->getSku();
        }

        $select = $read->select()
            ->from('amazon_product_item')
            ->where('product_type = ?', 'configurable');

        if (is_array($skus) && count($skus))
            $select->where("asin NOT IN (?)", $skus);

        $ConfigProducts = $read->fetchAll($select);

        foreach ($ConfigProducts as $oneConfigProduct) {

            if (!isset($oneConfigProduct["asin"]) || !isset($oneConfigProduct["item_attributes"])) continue;

            $configProductParams = array();

            $decodedConfigProductParams = json_decode($oneConfigProduct['item_attributes']);

            $configProductMainData['title'] = $decodedConfigProductParams->Title;
            $configProductMainData['asin'] = $oneConfigProduct['asin'];

            $confProduct = $this->_createProduct(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE, false, $configProductParams, null, $configProductMainData, null); // create conf product but do not save

            //Set affiliate link for config product
            if (isset($oneConfigProduct['affiliate_link'])) {
                $confProduct->setAffiliateLink($oneConfigProduct['affiliate_link']);
            }

            /*Get all simple for config*/
            $simpleProductData = new Mage_Core_Model_Resource();
            $read = $configPoductdta->getConnection('core_read');
            $select = $read->select()
                ->from('amazon_product_item')
                //->where("asin NOT IN (?)", $skus)
                ->where('product_type = ?', 'simple')
                ->where('parent_asin=?', $oneConfigProduct['parent_asin'])
                ->limit(20);

            $simpleProducts = $read->fetchAll($select);
            $configurableProductsData = array();

            foreach ($simpleProducts as $oneSimpleProduct) {
                if (!isset($oneSimpleProduct['item_attributes'])
                    || !isset($oneSimpleProduct['variation_attributes'])
                    || !isset($oneSimpleProduct["offers"])
                ) continue;

                /*get offers data*/
                $offersData = json_decode($oneSimpleProduct['offers']);

                $simpleProductAttributesVariation = json_decode($oneSimpleProduct['variation_attributes']);
                $simpleProductAttributesItem = json_decode($oneSimpleProduct['item_attributes']);
                $simpleProductAttributesMain['asin'] = $oneSimpleProduct['asin'];

                //Continue if product have not a price
                if (!isset($offersData->Offer->OfferListing->Price->FormattedPrice)) continue;
                else {
                    $_simpleProductAttributesPriceParam['FormattedPrice'] = $offersData->Offer->OfferListing->Price->FormattedPrice;
                    $simpleProductAttributesPriceParam['FormattedPrice'] = str_replace('$', '', $_simpleProductAttributesPriceParam['FormattedPrice']);
                    @$simpleProductAttributesPriceParam['Amount'] = $_simpleProductAttributesPriceParam['Amount'];
                }
                $_tem = json_decode($oneSimpleProduct['large_images']);
                @$simpleProductAttributesPriceParam['URL'] = $_tem->URL;


                $simpleProduct = $this->_createProduct(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE, true, $simpleProductAttributesItem, $simpleProductAttributesVariation, $simpleProductAttributesMain, $simpleProductAttributesPriceParam, $oneConfigProduct['affiliate_link'] ); // create simple product
                // $confProduct = $this->_createProduct(Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE, false); // create conf product but do not save

                // associate simple (child) product to the configurable (parent) product.  There are several options, but not all of them works well
                // with recent CE 1.9+ Magento versions
                // We will show you the easiest one. Just follow 5 next steps:
                //Set affiliate link for simple product

                if (isset($oneConfigProduct['affiliate_link'])) {
                    $simpleProduct->setAffiliateLink($oneConfigProduct['affiliate_link']);

                }

                // 1. select configurable attributes
                $colorAttributeId = Mage::getModel('eav/entity_attribute')->getIdByCode(Sereban_Product_ProductController::CATALOG_PRODUCT, 'color');
                $confProduct->getTypeInstance()->setUsedProductAttributeIds(array($colorAttributeId));

                // 2. prepare information for each simple product

                $configurableAttributesData = $confProduct->getTypeInstance()->getConfigurableAttributesAsArray();

                $simpleProductsData = array(

                    'label' => $simpleProduct->getAttributeText('color'),
                    'attribute_id' => $colorAttributeId,
                    'value_index' => (int)$simpleProduct->getColor(),
                    'is_percent' => 0,
                    'pricing_value' => $simpleProduct->getPrice(),
                );

                $confProduct->setPrice($simpleProduct->getPrice());
                $configurableProductsData[$simpleProduct->getId()] = $simpleProductsData;
                $configurableAttributesData[0]['values'][] = $simpleProductsData;

                // 3. set data in 2 required formats
                $confProduct->setConfigurableProductsData($configurableProductsData);
                $confProduct->setConfigurableAttributesData($configurableAttributesData);

                // 4. save product with special flag
                $confProduct->setCanSaveConfigurableAttributes(true);

            }
            try {
                $confProduct->save();

            } catch (Exception $e) {
                Mage::logException($e);
            }

        }

        $this->_redirectReferer();
    }


    protected function _createProduct($type, $doSave = true, $productData = array(), $attributes = array(), $mainData = array(), $price = array(), $affiliateLink = null)
    {

        // required for some versions
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $product = Mage::getModel('catalog/product');

        // set madatory system attributes
        //$rand = rand(1, 9999);
        $product
            ->setTypeId($type)// e.g. Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
            ->setAttributeSetId(4)// default attribute set
            ->setSku($mainData['asin'])// generate some random SKU
            ->setWebsiteIDs(array(1));

        // make the product visible
        $product
            ->setCategoryIds(array(2, 3))
            ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) // visible in catalog and search
        ;
        // configure stock

        $product->setStockData(array(
            'use_config_manage_stock' => 1, // use global config ?
            'manage_stock' => 0, // shoudl we manage stock or not?
            'is_in_stock' => 1,
            'qty' => 9999,
        ));

        // optimize performance, tell Magento to not update indexes
        $product
            ->setIsMassupdate(true)
            ->setExcludeUrlRewrite(true);

        if ($type == 'simple') {
            if (!is_null($productData) && !is_null($attributes)) {

                $mapping = array(
                    "Color" => "color",
                    "ClothingSize" => "size",
                    "Title" => "name",
                    "Manufacturer" => "manufacturer",
                    "Feature" => "description",
                    "Brand" => "brand",

                );

                foreach ($productData as $amazonName => $amazonValue) {

                    if (@$mapping[$amazonName]) {
                        if ($mapping[$amazonName] != 'name' && $amazonName !== 'Feature') {

                            $this->_addNewAttributeOption($mapping[$amazonName], $amazonValue);
                            $optionId = $this->_getOptionIDByCode($mapping[$amazonName], $amazonValue);
                            $product->setData($mapping[$amazonName], $optionId);

                        } elseif ($mapping[$amazonName] == 'name') {

                            $product->setData('name', $amazonValue);
                        }


                    }
                    if ($amazonName == 'Feature') {
                        $product->setData('description', implode(".", $amazonValue));
                    }
                }


            }

            // finally set custom data


            $product
                //->setShortDescription('description')// add text attribute
                // set up prices
                //->setPrice(24.50)
                //->setSpecialPrice(19.99)
                ->setTaxClassId(2)// Taxable Goods by default
                ->setWeight(87)
                ->setAffiliateLink($affiliateLink);

            if (!is_null($price)) {

                $product->setPrice($price['FormattedPrice']);

            }
            /*Add autocreating attribute*/

            if (!is_null($attributes)) {
                $mapping = array(
                    "Color" => "color",
                    "Size" => "size",

                );

                foreach ($attributes as $amazonName => $amazonValue) {

                    $this->_addNewAttributeOption($mapping[$amazonValue->Name], $amazonValue->Value);

                    $optionId = $this->_getOptionIDByCode($mapping[$amazonValue->Name], $amazonValue->Value);
                    $product->setData($mapping[$amazonValue->Name], $optionId);
                }

            }
        } elseif ($type == 'configurable') {

            $product->setName($mainData['title']);
            $product->setSku($mainData['asin']);
        }


        // add product images
        /** @var Mage_Catalog_Model_Product $product */
        if ($type == 'simple' && isset($price['URL'])) {
            try {
                $content = @file_get_contents($price['URL']);
                $parts = explode("/", $price['URL']);
                if ($parts) {
                    $imageName = $parts[count($parts) - 1];
                }

                if ($content && strlen($content) && isset($imageName)) {
                    $_path = self::PATH_TO_IMAGE . DS . $imageName;
                    file_put_contents($_path, $content);
                    $product->addImageToMediaGallery($_path, array("thumbnail", "small_image", "image"), false, false);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        if ($doSave) {
            try {

                $product->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }

        }


        return $product;
    }


    /**
     * @param $attrCode
     * @param $optionLabel
     * @return bool
     */
    protected function _getOptionIDByCode($attrCode, $optionLabel)
    {
        $attrModel = Mage::getModel('eav/entity_attribute');

        $attrID = $attrModel->getIdByCode(Sereban_Product_ProductController::CATALOG_PRODUCT, $attrCode);
        $attribute = $attrModel->load($attrID);

        $options = Mage::getModel('eav/entity_attribute_source_table')
            ->setAttribute($attribute)
            ->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $optionLabel) {
                return $option['value'];
            }
        }

        return false;
    }


    /**
     * Save configurable product relations
     *
     * @param Mage_Catalog_Model_Product|int $configProduct the parent id
     * @param array $simpleProductIds the children id array
     * @return Mage_Catalog_Model_Resource_Product_Type_Configurable
     */
    protected function _associateProduct($configProduct = null, $simpleProductIds = array())
    {

        Mage::getResourceModel('catalog/product_type_configurable')->saveProducts($configProduct, array($simpleProductIds));
    }



    /*@TODO Create generator of new options to exist product attribute*/
    /**
     * Add new attribute opton value if not exist
     * @param null $optionCode
     * @param null $optionValue
     * @return mixed
     * @throws Exception
     */
    public function _addNewAttributeOption($optionCode = null, $optionValue = null)
    {

        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');

        $attribute_id = $attribute_model->getIdByCode('catalog_product', $optionCode);
        $attribute = $attribute_model->load($attribute_id);

        $attribute_table = $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            // checking if already exists
            if ($option['label'] == $optionValue) {
                $optionId = $option['value'];
                return $optionId;
            }
        }

        $value['option'] = array($optionValue, $optionValue);
        $result = array('value' => $value);
        $attribute->setData('option', $result);
        $attribute->save();

    }

    /**
     * prepare array for associating with config product
     * @param $simpleProductId
     */
    protected function _prepateSimpleProductArray($simpleProductId)
    {
        if (isset ($simpleProductId)) {
            $this->_simpleProductsArray[] = $simpleProductId;
        }
    }

    /**
     *unset global array when onfig product created
     */
    protected function unsetSimpleProductArray()
    {
        $this->_simpleProductsArray = array();
    }


}