<?php
class Sereban_Amazon_Block_Adminhtml_System_Config_Synchronize_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * $element->getHint() -> function
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
       return '<button onclick="' . $element->getHint() . '(event)">' . $element->getButtonLabel() . '</button>';
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setButtonLabel($element->getLabel());
        $element->setLabel(null);
        return parent::render($element);
    }
}