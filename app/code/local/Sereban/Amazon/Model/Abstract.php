<?php
/** This class used like Treat for additonal functions */
abstract class Sereban_Amazon_Model_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * @return Sereban_Amazon_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("sereban_amazon");
    }
}