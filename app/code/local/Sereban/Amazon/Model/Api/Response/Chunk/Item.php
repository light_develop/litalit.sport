<?php
/**
 * Class Sereban_Amazon_Model_Api_Response_Chunk_Item
 * @method $this setProductType(string $type)
 * @method $this setAsin(string $asin)
 * @method string getAsin()
 */
class Sereban_Amazon_Model_Api_Response_Chunk_Item extends Sereban_Amazon_Model_Api_Response_Chunk_Abstract
{
    /** types of saving products  */
    const SIMPLE       = "simple";
    const CONFIGURABLE = "configurable";
    const PRIMARY_KEY  = "asin";

    protected function _construct() {
        $this->_init("sereban_amazon/api_response_chunk_item");
    }

    protected function _beforeSave() {
        $this->_addAdditionalFields();
        $this->getOrigData(); //catch if this item already exist -> in this case we should update it

        return parent::_beforeSave();
    }

    protected function _addAdditionalFields() {
        if(!$this->getOrigData("created_at")) {
            $this->setData("created_at", $this->_getHelper()->getTimestamp());
        }

        $this->setData("updated_at", $this->_getHelper()->getTimestamp());
    }
}