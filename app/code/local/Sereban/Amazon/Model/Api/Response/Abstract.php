<?php

/**
 * Class Sereban_Amazon_Model_Api_Response_Abstract
 * @method void setRequest(Sereban_Amazon_Model_Api_Request_Abstract $request)
 * @method Sereban_Amazon_Model_Api_Request_Abstract getRequest()
 */
abstract class Sereban_Amazon_Model_Api_Response_Abstract extends Sereban_Amazon_Model_Abstract
{
    /** 1 -> array; 2-> object */
    const RETURN_TYPE    = 1;
    const PATH_DELIMITER = "/";

    private $_responces       = array();
    /** @var  $_instanceName -> redefine this variable to "Item" for example */
    protected $_instanceName;

    /**
     * @param $path -> for example Items/Item
     * array | object $_response
     * @return array | object
     */
    protected function _loop($path, $_response) {
        if(!is_array($_response) && !is_object($_response)) return (self::RETURN_TYPE == 1) ? array() : new StdClass();
        $arrayPath     = explode(self::PATH_DELIMITER, $path); //array of all patch items

        while(count($arrayPath)) {
            $_item = array_shift($arrayPath); //new item
            if(!isset($_response[$_item]) &&!isset($_response->{$_item})) {
                Mage::log("Cannot access $_item in lookup response");
                return null;
            }

            switch(self::RETURN_TYPE) {
                case 1:
                    $_response = $_response[$_item];
                    break;
                case 2:
                    $_response = $_response->{$_item};
            }

        }

        return $_response;
    }
    /** this is andatory function which add new items from responses */
    abstract function reindex();

    public function getResponces() {
        return $this->_responces;
    }

    /**
     * @return false|Sereban_Amazon_Model_Api_Response_Chunk_Item
     */
    protected function _getInstance() {
        return Mage::getModel("sereban_amazon/api_response_chunk_$this->_instanceName");
    }
    /**
     * @param array|object $response
     */
    public function addResponse($response) {
        if(!is_array($response) && !is_object($response)) throw new Exception("Response must be an object or an array");
        $this->_responces[]      = $response;
    }

    public function unsetResponces() {
        $this->_responces = array();
    }
}