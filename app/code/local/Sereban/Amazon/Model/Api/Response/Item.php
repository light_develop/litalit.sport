<?php
class Sereban_Amazon_Model_Api_Response_Item extends Sereban_Amazon_Model_Api_Response_Abstract
{
    const ITEM_SERIALIZED_PATH       = "amazon/lookup/item/serialized";
    const ITEM_UNSERIALIZED_PATH     = "amazon/lookup/item/unserialized";
    /** patches for products */
    const CONFIGURATION_PRODUCT_PATH = "Items/Item"; //for parent asin
    const SIMPLE_PRODUCT_PATH        = "Variations/Item";
    const PRIMARY_KEY                = "ASIN";
    /** @var  Sereban_Amazon_Model_Api_Response_Chunk_Item */
    protected $_itemsCollection;
    protected $_instanceName         = "item";
    public $grabParents              = false;

    protected function _processSimples($configurable) {
        if(in_array(Sereban_Amazon_Model_Api_Request_Abstract::RESPONSE_GROUP_VARIATIONS,
            $this->getRequest()->getResponseGroups())) {
            $_simpleProduct   = $this->_loop(self::SIMPLE_PRODUCT_PATH, $configurable);

            if($this->_hasMultipleItems($_simpleProduct)) {
                foreach($_simpleProduct as $product) {
                    $this->_prepareItem($product);
                }
            } else {
                $this->_prepareItem($_simpleProduct);
            }

        }
    }

    /**
     * @return object|Sereban_Amazon_Model_Resource_API_Response_Chunk_Item_Collection
     */
    public function reindex() {
        $this->_itemsCollection = $this->_getInstance()->getCollection(); //prepare collection
        $_responces     = $this->getResponces();

        foreach($_responces as $response) {
            $_configurableProduct = $this->_loop(self::CONFIGURATION_PRODUCT_PATH, $response);

            /** Process configurable products */
            if($this->_hasMultipleItems($_configurableProduct)) {
                foreach($_configurableProduct as $product) {
                    $this->_prepareItem($product);
                    $this->_processSimples($product);
                }
            } else {
                $this->_prepareItem($_configurableProduct);
                $this->_processSimples($_configurableProduct);
            }
        }

        $this->unsetResponces(); //clear Responces

        return $this->_itemsCollection;
    }

    protected function _parentFromChild($product) {
        if($this->_loop("ASIN", $product) != $this->_loop("ParentASIN", $product)) { //if we have simple but not have configurable
            try {
                $parentAsin = $this->_loop("ParentASIN", $product);
                $_item = $this->_getInstance();

                if(!$_item->load($parentAsin, "parent_asin")->getAsin()) {
                    $this
                        ->_getInstance()
                        ->setProductType("configurable")
                        ->setAsin($this->_loop("ParentASIN", $product))
                        ->save();
                }
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }
    }

    protected function _prepareItem($product) {
        $type = ($this->_loop("ASIN", $product) != $this->_loop("ParentASIN", $product)) ? "simple" : "configurable";
        /** In this place configurables that we got from simples saved in database */
        if($this->grabParents) {
            $this->_parentFromChild($product);
        } else {
            if($item = $this->_getInstance()) {
                // process array as json field
                foreach($this->_getHelper()->getPathConfig(self::ITEM_SERIALIZED_PATH) as $magentoParam => $amazonParam) {
                    $item->setData($magentoParam,
                        $this->_getHelper()->jsonEncode($this->_loop($amazonParam, $product)));
                }
                // process string as VARCHAR in database
                foreach($this->_getHelper()->getPathConfig(self::ITEM_UNSERIALIZED_PATH) as $magentoParam => $amazonParam) {
                    $item->setData($magentoParam, $this->_loop($amazonParam, $product));
                }
            }

            $item->setProductType($type);

            /** trying to save item */
            try {
                $item->save();
                //$this->_itemsCollection->addItem($item);
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }
    }

    public function grabAsins() {

    }

    /**
     * @param array | object $array
     * @return bool
     */
    protected function _hasMultipleItems($arrayObject) {
        return isset($arrayObject[0]);
    }
}