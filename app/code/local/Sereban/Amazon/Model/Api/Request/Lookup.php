<?php

/**
 * Class Sereban_Amazon_Model_Api_Request_Lookup
 * @method $this setAsin($asin)
 * @method array|int getAsin()
 *
 */
class Sereban_Amazon_Model_Api_Request_Lookup extends Sereban_Amazon_Model_Api_Request_Abstract
{
    const MAX_SIMILARITIES_ITERATIONS = 10;
    const MAX_SIMILARITIES_DEPTH      = 3;
    const MIN_SIMILARITIES_DEPTH      = 1;
    const MIN_SIMILARITIES_ITERATIONS = 2;
    const ALLOWED_LOOKUPS             = 100; //max allowed not more than 50 products

    protected $_responseGroups   = array(
        self::RESPONSE_GROUP_SMALL,
        self::RESPONSE_GROUP_VARIATIONS
    );
    protected $_responseInstance = "item";
    /**
     * @param int $asin
     * @param bool $similarity -> if use similarity -> grab similar items
     * @throws Exception
     */
    public function lookUp($similarity = false) {
        $asin = $this->getAsin();
        if(is_array($asin)) {
            foreach($asin as $index => $_asin) {
                if($index > self::ALLOWED_LOOKUPS) break;
                $this->_getResponse()->addResponse($this->_lookup($_asin));
            }
        } else {
            $this->_getResponse()->addResponse($this->_lookup($asin));
        }

        $this->_getResponse()->reindex();
    }

    /**
     * @param $asin
     * @return array|object
     * @throws Exception
     */
    protected function _lookup($asin) {
        $this->_validateAsin($asin);
        return $this->_getECS()->lookup($asin);
    }

    /**
     * Used To loop throw all similarities
     * Use @var int $depth -> How many similarities of similarities we want to take
     * @var int $iterations
     * @param $asin
     * @throws Exception
     */
    public function similarLookup($asin) {
        /** @var Sereban_Amazon_Model_Resource_API_Response_Chunk_Item_Collection $similarityCollection */
        $similarityCollection = Mage::getModel("sereban_amazon/api_response_chunk_item")->getCollection();
        $this->_validateAsin($asin);
        /** @var  Sereban_Amazon_Model_Api_Response_Chunk_Item $_item */
        $_item = Mage::getModel("sereban_amazon/api_response_chunk_item");
        $similarityCollection->addItem($_item->setAsin($asin)); //prepare collection

        $iterations = ($this->_getHelper()->getSimilarityIterations())
                            ? $this->_getHelper()->getSimilarityIterations() : self::MIN_SIMILARITIES_ITERATIONS;

        $depth      = ($this->_getHelper()->getSimilarityDepth())
                            ? $this->_getHelper()->getSimilarityDepth() : self::MIN_SIMILARITIES_DEPTH;

        if($iterations > self::MAX_SIMILARITIES_ITERATIONS)
                $iterations = self::MAX_SIMILARITIES_ITERATIONS;

        if($depth > self::MAX_SIMILARITIES_DEPTH)
                $depth      = self::MAX_SIMILARITIES_DEPTH;

        for($d = 0; $d <= $depth; $d++) {
            $asins = array();
            foreach($similarityCollection as $item) {
                /** @var Sereban_Amazon_Model_Api_Response_Chunk_Item $item */
                $asins[] = $item->getAsin();
            }

            $similarityCollection = $this->_similarityLookup($iterations, implode(self::DELIMITER, $asins));
        }
    }

    protected function _similarityLookup($iterations, $asin) {
        for($i = 0; $i <= $iterations; $i++) { //process all iterations
            $this->_getResponse()->addResponse($this->_getECS()->similarityLookup($asin));
        }

        return $this->_getResponse()->reindex();
    }

    /**
     * @param int $asin
     * TODO: add Additional Validation
     * @throws Exception
     */
    protected function _validateAsin($asin) {
        if(empty($asin)) throw new Exception("Assin is empty");
    }

    protected function _prepareLookupQuery() {

    }
}