<?php
class Sereban_Amazon_Model_Api_Request_Search extends Sereban_Amazon_Model_Api_Request_Abstract
{
    const PAGE_ALLOW_QTY = 10;

    protected $_responseInstance = "item";
    protected $_responseGroups = array(
        self::RESPONSE_GROUP_LARGE,
        self::RESPONSE_GROUP_VARIATIONS,
        self::RESPONSE_GROUP_SIMILARITIES
    );

    /**
     * Search with Amazon api by category and word
     * @throws Exception
     */
    public function search() {
        // go throught all allowed items
        for($i = 1; $i < self::PAGE_ALLOW_QTY; $i++) {
            $response = $this->_getECS()
                ->category($this->_getHelper()->getSearchCategory())
                ->page($i)
                ->search($this->_getHelper()->getSearchWord());
            /** Add responces from all pages */
            $this->_getResponse()->addResponse($response);
        }
    }
}