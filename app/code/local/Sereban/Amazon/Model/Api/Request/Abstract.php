<?php

/**
 * Class Sereban_Amazon_Model_Api_Abstract
 * @method Sereban_Amazon_Model_Api_Response_Item getResponse()
 * This class used like abstract for REST and SOAP api`s
 */
abstract class Sereban_Amazon_Model_Api_Request_Abstract extends Sereban_Amazon_Model_Abstract
{
    const REQUEST_CONDITIONS          = "All";
    const DELIMITER                   = ",";
    /** used to retrieve all data from collection */
    const RESPONSE_GROUP_LARGE        = "Large";
    const RESPONSE_GROUP_SMALL        = "Small";
    /** used to retrieve simple products */
    const RESPONSE_GROUP_VARIATIONS   = "Variations";
    const RESPONSE_GROUP_SIMILARITIES = "Similarities";

    protected $_responseGroups        = array();
    protected $_responceInstance;
    /**
     * @return Zend_Http_Client
     */
    protected function _getZendHTTPClient() {
        return new Zend_Http_Client();
    }
    /**
     * @return array
     */
    public function getResponseGroups() {
        return $this->_responseGroups;
    }
    /**
     * @return false|Sereban_Amazon_Model_Api_Response_Item
     */
    public function _getResponse() {
        return Mage::getSingleton("sereban_amazon/api_response_$this->_responseInstance")->setRequest($this);
    }

    /**
     * Flushing all response Groups
     */
    public function flushResponseGroups() {
        $this->_responseGroups = array();
    }

    /**
     * @param $responseGroup
     */
    public function addResponseGroup($responseGroup) {
        $this->_responseGroups[] = $responseGroup;
    }

    /**
     * @return Amazon_ECS
     * @throws Exception
     */
    protected function _getECS() {
        $_amazonECS = new Amazon_ECS(
            $this->_getHelper()->getPublicKey(),
            $this->_getHelper()->getPrivateKey(),
            $this->_getHelper()->getECSCountry(),
            $this->_getHelper()->getAssociatedTag()
        );
        /** set respons size */

        $_amazonECS
            ->responseGroup(implode(self::DELIMITER, $this->_responseGroups))
            ->optionalParameters(array(
                "Condition" => self::REQUEST_CONDITIONS
            ))
            ->returnType(Sereban_Amazon_Model_Api_Response_Abstract::RETURN_TYPE);

        return $_amazonECS;
    }

}