<?php
class Sereban_Amazon_Model_Resource_API_Response_Chunk_Item_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct() {
        $this->_init("sereban_amazon/api_response_chunk_item");
    }
}