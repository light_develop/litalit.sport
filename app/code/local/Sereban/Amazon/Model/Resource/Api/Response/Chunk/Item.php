<?php

/**
 * Class Sereban_Amazon_Model_Resource_API_Response_Chunk_Item
 * @method string getAsin()
 */
class Sereban_Amazon_Model_Resource_API_Response_Chunk_Item extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_isPkAutoIncrement = false;

    protected function _construct() {
        $this->_init("sereban_amazon/amazon_item", "asin");
    }
}