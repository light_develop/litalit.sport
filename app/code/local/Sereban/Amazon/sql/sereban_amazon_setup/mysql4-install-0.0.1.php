<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
/** @var string $amazonItemTable -> table for amazon items */
$amazonItemTable = $installer->getTable("amazon_product_item");

$installer->startSetup();

$installer->run("CREATE TABLE {$amazonItemTable} (
                           `asin`     VARCHAR(255) PRIMARY KEY NOT NULL,
                           `parent_asin` VARCHAR(255),
                           `small_images` TEXT, #json
                           `medium_images` TEXT, #json
                           `large_images` TEXT, #json
                           `product_type` VARCHAR(100),
                           `item_attributes` TEXT, #json,
                           `offers`         TEXT,
                           `variation_attributes` TEXT,
                           `created_at` DATETIME,
                           `updated_at` DATETIME
                      )");

$installer->endSetup();

