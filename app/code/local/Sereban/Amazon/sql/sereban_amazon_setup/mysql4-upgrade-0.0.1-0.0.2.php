<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$amazonItemTable = $installer->getTable("amazon_product_item");

$installer->startSetup();

$this->run("
ALTER TABLE {$amazonItemTable}
        ADD COLUMN `affiliate_link` TEXT
");

$installer->endSetup();