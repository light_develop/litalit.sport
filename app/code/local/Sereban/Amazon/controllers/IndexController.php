<?php

class Sereban_Amazon_IndexController extends Mage_Core_Controller_Front_Action
{
    protected $_searchString = "http://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Dfashion&field-keywords=sports&rh=n%3A7141123011%2Ck%3Asports";

    const DATA_ASIN     = "data-asin";
    const PAGE_NUMBER   = 10;
    const SEARCH_TYPE   = "simple";
    const MAXIMUM_ASISN = 10;

    /** Dummy version */
	public function importAction(){
        if($this->getRequest()->getParam("action") == "create") {
            $this->_curlRequest();
            $this->_parseConfigurables();
            $this->_update();
            $this->_update("affiliate_link", "simple"); //affiliate link for simples
            Mage::getSingleton("adminhtml/session")->addSuccess("Syncrhonize Products successfully");
            $this->_redirectReferer();
        }  else {
           // $this->_update();
            $this->_update("affiliate_link", "simple", "Small"); //affiliate link for simples
            Mage::getSingleton("adminhtml/session")->addSuccess("Syncrhonize Products successfully");
            $this->_redirectReferer();
        }

	}

    protected function _parseConfigurables() {
        $items = $this
            ->_getChunkItem()
            ->getCollection()
            ->addFieldToFilter("item_attributes", array("null" => true));
        $asins = $this->_getAsins($items);

        $_request = $this->_getRequestLookup();
        $_request->flushResponseGroups();
        $_request->addResponseGroup("Small");
        $_response = $_request->_getResponse();
        $_response->grabParents = true;
        $_request->setAsin($asins);
        $_request->lookUp(false);
    }

    /**
     * @param Sereban_Amazon_Model_Resource_API_Response_Chunk_Item_Collection $collection
     * @return array
     */
    protected function _getAsins(Sereban_Amazon_Model_Resource_API_Response_Chunk_Item_Collection $collection) {
        $asins          = array();
        $counter        = 0;

        foreach($collection as $item) {
            $asins[$counter++ % ceil($collection->count() / self::MAXIMUM_ASISN)][] = $item->getAsin();
        }

        array_walk($asins, function($value, $key) use (&$asins){
            $asins[$key] = implode(",", $value);
        });

        return $asins;
    }

    protected function _update($nullPart = "item_attributes", $type = "configurable", $responseGroup = "Variations") {
        $items = $this
            ->_getChunkItem()
            ->getCollection()
            ->addFieldToFilter("product_type", $type)
            ->setPageSize(200);
        /** @var Sereban_Amazon_Model_Resource_Api_Response_Chunk_Item_Collection $items */
        $items
            ->getSelect()
            ->where("$nullPart IS NULL");

        $asins = $this->_getAsins($items);

        $_request = $this->_getRequestLookup();
        $_request->addResponseGroup($responseGroup);
        $_response = $_request->_getResponse();
        $_response->grabParents = false;
        $_request->setAsin($asins);
        $_request->lookUp(false);
    }

    /**
     * @return Sereban_Amazon_Model_Api_Request_Lookup
     */
    protected function _getRequestLookup() {
        return Mage::getSingleton("sereban_amazon/api_request_lookup");
    }

    /**
     * @return Sereban_Amazon_Model_Api_Response_Chunk_Item
     */
    protected function _getChunkItem() {
        return Mage::getSingleton("sereban_amazon/api_response_chunk_item");
    }

    /**
     * @return Sereban_Amazon_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper("sereban_amazon");
    }

    /**
     * Just go by curl and parse all asins
     */
    protected function _curlRequest() {
        $start = $this->_getHelper()->getStartPage();
        $end   = $this->_getHelper()->getEndPage();

        for($i= $start; $i <= $end; $i++) {
            $ch = curl_init();
            $_link = $this->_searchString . "&page=$i";
            curl_setopt($ch, CURLOPT_URL, $_link);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            $output = curl_exec($ch);
            curl_close($ch);

            $dom = new DOMDocument();
            @$dom->loadHTML($output);

            $xpath = new DOMXPath($dom);
            $result = $xpath->query("//*[@" . self::DATA_ASIN . "]");

            if(!$result || !$result instanceof DOMNodeList) {
                Mage::log("Unable to parse url : ". $_link);
                continue;
            }

            for ($_it = 0; $_it < $result->length; $_it++) {
                $item      = $result->item($_it);
                if($item instanceof DOMElement) {
                    $attribute = $item->getAttribute(self::DATA_ASIN);
                    $attribute = preg_replace("/\"/", "", $attribute);

                    if($attribute) {
                        try {
                            /** save asins to table */
                            $this->_getChunkItem()
                                ->setAsin($attribute)
                                ->setProductType(self::SEARCH_TYPE)
                                ->save();

                        } catch(Exception $e) {
                            Mage::logException($e);
                        }
                    }
                }
            }

        }
    }

    public function updateLinksAction() {
        $items = $this->_getChunkItem()
            ->getCollection();
        $skuData = array();

        foreach($items as $item) {
            $skuData[$item["asin"]] = $item["affiliate_link"];
        }

        $products = Mage::getSingleton("catalog/product")->getCollection();


        foreach($products as $product) {
            $sku = $product->getSku();

            if(isset($skuData[$sku]) && strlen($skuData[$sku])) {
                $product->setData("affiliate_link", $skuData[$sku]);
            } else {
                $product->setVisibility(1);
            }

            try {
                $product->save();
            } catch(Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }
}