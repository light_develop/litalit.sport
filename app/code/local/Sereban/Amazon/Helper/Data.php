<?php
class Sereban_Amazon_Helper_Data extends Mage_Core_Helper_Data
{
    const AMAZON_PATH      = 'sereban_amazonconfig';
    const CREDENTIALS_PATH = "credentials";
    /** mean usa -> amazon.com */
    const DEFAULT_COUNTRY  = "com";

    /**
     * Return any config from Sereban_Amazon scope
     * @param $group
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getConfig($group, $field, $storeId = null) {
        return Mage::getStoreConfig(self::AMAZON_PATH . "/" . $group . "/" . $field, $storeId);
    }

    /**
     * @param $path
     * @return array|string
     */
    public function getPathConfig($path) {
        return Mage::app()->getConfig()->getNode($path)->asArray();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getPublicKey() {
        $_publicKey = $this->getConfig(self::CREDENTIALS_PATH, "publickey");
        if(empty($_publicKey)) {
            throw new Exception("Please enter your public key from amazon API");
        }

        return $_publicKey;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getPrivateKey() {
        $_privateKey = $this->getConfig(self::CREDENTIALS_PATH, "privatekey");
        if(empty($_privateKey)) {
            throw new Exception("Please enter your private key from amazon API");
        }

        return $_privateKey;
    }

    /**
     * @return mixed
     */
    public function getAssociatedTag() {
        return $this->getConfig(self::CREDENTIALS_PATH, "associatedtag");
    }

    /**
     * @return mixed
     */
    public function getECSCountry() {
        return $this->getConfig(self::CREDENTIALS_PATH, "country");
    }

    /**
     * @return Zend_Date
     */
    public function _getZendDate() {
        return new Zend_Date();
    }

    /**
     * @return int
     */
    public function getTimestamp() {
        return $this->_getZendDate()->getTimestamp();
    }

    /**
     * @return mixed
     */
    public function getSearchCategory() {
        return $this->getConfig("search_settings", "category");
    }

    /**
     * TODO: create few words
     * @return mixed
     */
    public function getSearchWord() {
        return $this->getConfig("search_settings", "word");
    }

    /**
     * More than 2 -> take similar products from similar
     * @return mixed
     */
    public function getSimilarityDepth() {
        return $this->getConfig("search_settings", "similarity_depth");
    }

    /**
     * Get different randoms products from x iterations
     * @return mixed
     */
    public function getSimilarityIterations() {
        return $this->getConfig("search_settings", "similarities_iterate");
    }

    public function getStartPage() {
        return $this->getConfig("management", "from_page") != null ? $this->getConfig("management", "from_page") : 1;
    }

    public function getEndPage() {
        return $this->getConfig("management", "to_page") != null ? $this->getConfig("management", "to_page") : $this->getStartPage() + 1;
    }
}